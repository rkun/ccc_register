package main

import (
  "fmt"
  "github.com/gin-gonic/gin"
  "github.com/jinzhu/gorm"
  "errors"
  _ "github.com/jinzhu/gorm/dialects/sqlite"
)

type User struct {
  gorm.Model
  Name string `json:"name" gorm:"primary_key"`
  Grade string `json:"grade"`
  Role string `json:"role"`
  Transactions []Transaction `json:"transactions"`
}

type Product struct {
  gorm.Model
  Name string `json:"name" gorm:"primary_key"`
  Price int `json:"price"`
  Purchases []Purchase `json:"purchases"`
}

type Purchase struct {
  gorm.Model
  ProductID uint `json:"product_id"`
  TransactionID uint `json:"transaction_id"`
  Count uint `json:"count"`
}

type Transaction struct {
  gorm.Model
  UserID int `json:"user_id"`
  Purchases []Purchase `json:"purchases"`
}

func InitDB() *gorm.DB {
  db, err := gorm.Open("sqlite3", "test.db")
  if err != nil {
	panic("Can't connect server")
  }
  // スキーマをマイグレーション
  db.CreateTable(&User{})
  db.AutoMigrate(&User{})
  db.CreateTable(&Product{})
  db.AutoMigrate(&Product{})
  db.CreateTable(&Purchase{})
  db.AutoMigrate(&Purchase{})
  db.CreateTable(&Transaction{})
  db.AutoMigrate(&Transaction{})
  return db
}

func connect(id, pass string) error {
  return errors.New("It is message")
}

func main() {
  db := InitDB()
  router := gin.Default()
  router.GET("/", func(ctx *gin.Context){
	ctx.Status(200)
  })

  // ユーザー追加
  router.POST("/api/v1/user", func(ctx *gin.Context){
	var user User
	if err := ctx.BindJSON(&user); err != nil {
	  ctx.Status(400)
	  return
	}
	db.Create(&user)
	ctx.JSON(200, user)
	return
  })

  // ユーザー取得
  router.GET("/api/v1/user/:id", func(ctx *gin.Context){
	id := ctx.Param("id")
	var user User
	if err := db.Limit(1).Find(&user, "id = ?", id).Error; err != nil {
	  print(err)
	  ctx.Status(404)
	  return
	}
	db.Model(&user).Related(&user.Transactions)
	ctx.JSON(200, user)
  })

  // ユーザ一一覧
  router.GET("/api/v1/users", func(ctx *gin.Context){
	var users []User
	if err := db.Find(&users).Error; err != nil {
	  ctx.Status(400)
	  return
	}
	for i := range users {
	  db.Model(users[i]).Related(&users[i].Transactions)
	}
	ctx.JSON(200, users)
	return
  })

  // 購入処理
  router.POST("/api/v1/transaction", func(ctx *gin.Context){
	var transaction Transaction
	if err := ctx.BindJSON(&transaction); err != nil {
	  ctx.Status(400)
	  return
	}

	db.Create(&transaction)
	ctx.JSON(200, transaction)
	return
  })

  router.POST("/api/v1/purchase", func(ctx *gin.Context){
	var purchase Purchase
	if err := ctx.BindJSON(&purchase); err != nil {
	  ctx.Status(400)
	  return
	}

	db.Create(&purchase)
	ctx.JSON(200, purchase)
	return
  })

  // 購入情報取得
  router.GET("/api/v1/transaction/:id", func(ctx *gin.Context){
	id := ctx.Param("id")
	var transaction Transaction
	if err := db.Limit(1).Find(&transaction, "id = ?", id).Related(&transaction.Purchases).Error; err != nil {
	  print(err)
	  ctx.Status(404)
	  return
	}

	ctx.JSON(200, transaction)
  })

  // 購入情報一覧
  router.GET("/api/v1/transactions", func(ctx *gin.Context){
	var transactions []Transaction
	if err := db.Find(&transactions).Error; err != nil {
	  ctx.Status(400)
	  return
	}
	for i := range transactions {
	  db.Model(&transactions[i]).Related(&transactions[i].Purchases)
	}
	ctx.JSON(200, transactions)
	return
  })

  // Purchase一覧
  router.GET("/api/v1/purchases", func(ctx *gin.Context){
	purchases := make([]Purchase, 10)
	if err := db.Find(&purchases).Error; err != nil {
	  ctx.Status(400)
	  return
	}
	ctx.JSON(200, purchases)
	return
  })

  // 商品追加
  router.POST("/api/v1/product", func(ctx *gin.Context){
	var product Product
	if err := ctx.BindJSON(&product); err != nil {
	  ctx.Status(400)
      fmt.Println(err)
	  return
	}

	db.Create(&product)
	ctx.JSON(200, product)
	return
  })

  // 商品削除
  router.DELETE("/api/v1/product/:id", func(ctx *gin.Context){
	id := ctx.Param("id")
	var product Product
	if err := db.Where("id = ?", id).First(&product).Error; err != nil {
      fmt.Println(err)
	  ctx.Status(404)
	  return
	}
	db.Delete(&product)
	ctx.JSON(200, product)
  })

  // 商品取得
  router.GET("/api/v1/product/:id", func(ctx *gin.Context){
	id := ctx.Param("id")
	var product Product
	if err := db.Where("id = ?", id).First(&product).Error; err != nil {
	  print(err)
	  ctx.Status(404)
	  return
	}
	ctx.JSON(200, product)
  })

  // 商品一覧取得
  router.GET("/api/v1/products", func(ctx *gin.Context){
	products := make([]Product, 10)
	if err := db.Find(&products).Error; err != nil {
	  ctx.Status(400)
	  return
	}
	ctx.JSON(200, products)
	return
  })

  router.GET("/api/v1/analytics", func(ctx *gin.Context) {
	var rows []struct {
	  ID uint `json:"ID"`
	  Name string `json:"name"`
	  Price uint `json:"price"`
	  Total uint `json:"total"`
	}
	db.Table("products").Select("products.id, products.name, products.price, sum(purchases.count) as total").Joins("join purchases on purchases.product_id = products.id").Group("products.id").Scan(&rows)
	fmt.Println(rows)
	ctx.JSON(200, rows)
  });

  router.Run()
}
